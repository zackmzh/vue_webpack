import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Hello2 from '@/components/Hello2'
import Add from '@/components/Add'
import Filter from '@/components/Filter'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: HelloWorld
    },
    {
      path: '/hello2',
      name: 'Hello2',
      component: Hello2
    },
    {
      path: '/add',
      name: 'Add',
      component: Add
    },
    {
      path: '/filter',
      name: 'Filter',
      component: Filter
    }
  ]
})
